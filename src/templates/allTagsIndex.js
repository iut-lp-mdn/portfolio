import React from "react"
import Header from '../components/Header'

const AllTagsTemplate = ({ pageContext }) => {
    const { tags } = pageContext
    return (
        <div>
            <Header/>
            <div className="container-fluid atcontainer center">
                <div>
                    <h3 className="text-center mt-5">Langages utilisées pour mes projets</h3>
                    <div className="mt-4">
                        {tags.map((tagName, index) => {
                            return (
                                <div className="d-flex justify-content-center" key={index}>
                                    <a href={`/tags/${tagName}`} className="btn btn-dark customButton mt-3">{tagName}</a>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>

    )
}

export default AllTagsTemplate