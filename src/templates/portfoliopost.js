import React from 'react'
import Head from '../components/Head'
import Footer from '../components/Footer'
import { graphql } from 'gatsby'

const Template = ({ data, pageContext }) => {
    const { next, prev } = pageContext
    const { markdownRemark } = data
    const title = markdownRemark.frontmatter.title
    const html = markdownRemark.html
    return (
        <div>
            <Head />
            <div className="container-fluid pjcontainer">
                <div className="row mt-5">
                    <div className="col-1"></div>
                    <div className="col-10">
                        <div className="card pjcard">
                            <div className="card-body">
                                <h1 className="text-center black pt-5">{title}</h1>
                                <div className="portfolioposts pt-5"
                                    dangerouslySetInnerHTML={{ __html: html }}
                                    style={{ fontFamily: 'avenir' }}
                                />
                                <br />
                                <div className="row">
                                    <div className="col-3 text-center">
                                        {prev && <a href={prev.frontmatter.path}><button type="button" class="btn btn-outline-secondary">Projet précédent</button></a>}
                                    </div>
                                    <div className="col-5"></div>
                                    <div className="col-4 text-center">
                                        {next && <a href={next.frontmatter.path}><button type="button" class="btn btn-outline-secondary">Projet suivant</button></a>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-1"></div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export const query = graphql`
    query($pathSlug: String!) {
        markdownRemark(frontmatter: {path: {eq: $pathSlug}}) {
            html
            frontmatter {
                title
            }
        }
    }
`

export default Template