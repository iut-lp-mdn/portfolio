import React from "react"
import Header from '../components/Header'

const SingleTagTemplate = ({ pageContext }) => {
    const { posts, tagName } = pageContext
    return (
        <div>
            <Header />
            <div className="container-fluid atcontainer center">
                <div>
                    <h3 className="text-center mt-5">Projets comportant du {`${tagName}`}</h3>
                    <div className="mt-4">
                        {posts.map((post, index) => {
                            return (
                                <div className="d-flex justify-content-center" key={index}>
                                    <a href={`${post.frontmatter.path}`} className="btn btn-dark customButton mt-3">{post.frontmatter.title}</a>
                                </div>
                            )
                        })}
                    </div>
                </div>

            </div>
        </div>
    )
}

export default SingleTagTemplate
