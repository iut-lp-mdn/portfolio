---
path: "/brobock"
date: "2019"
title: "Bro'Bock"
image: "brobock"
tags: ["PHP", "HTML", "CSS", "SQL"]
exerpt: "Un avant-goût de mon projet"
description: "Projet réalisé durant ma première année de BTS SIO"
---

### Debut du projet :

Nous sommes 3 à avoir réalisé ce projet. L'idée du projet nous est venu lors d'un cours. Le principe était de faire un site web qui permettait de consulter un catalogue de bière, l'administrateur pourrait ajouter / supprimer / modifier les bières tandis que l'utilisateur basique pourrait uniquement regarder les bières pour pouvoir les commander sur un site E-commerce (notre site a pour but d'être un site vitrine, non un site d'E-commerce)

Page d'accueil : ![imgaccueil](images/brobock2.png)

Page Histoire de la bière : ![imgaccueil](images/brobock3.png)

Page de connexion : ![imgconnexion](images/brobock4.png)

Page d'affichage des bières : ![imgconnexion](images/brobock5.png)

Page de gestion des marques et des catégories (même design) : ![imgconnexion](images/brobock7.png)

#### Base de données :

La base de données comporte 7 tables qui nous permettent de gérer les bières et les comptes des visiteurs.

Voici une photo du mcd :

![mcd](images/MCD Brobock.png)

### Suite et fin du projet :

Durant les semaines de travail autour de ce projet, nous avons réussi à faire le site comme on le souhaiter.

Vous pouvez aussi voir le code sur [github](https://github.com/Worgueno43/Brobock.git)

