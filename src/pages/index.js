import React from "react"
import Header from '../components/Header'
import Main from '../components/Main'
import Contact from '../components/Contact'
import Footer from '../components/Footer'
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Brobock from "./ImageP/brobock.png";
import AgenDa from "./ImageP/agenda.png";
import projectM from "./ImageP/projectM.png";

const page = ({ data }) => {
    const { edges } = data.allMarkdownRemark
    return (
        <Layout>
            <div>
                <Header />
                <Main />
                <div className="container-fluid wcontainer" id="project">
                    <h3 className="text_b text-center white">Mes Projets</h3>
                    <div className="row pt-2">
                        {edges.map(edge => {
                            const { frontmatter } = edge.node
                            return (
                                <div className="col-sm-4 mt-2" key={frontmatter.path}>
                                    <div class="card border-0 custom-card-P">
                                        {frontmatter.title === "Bro'Bock" && <img src={Brobock} class="card-img-top" alt={frontmatter.title} />}
                                        {frontmatter.title === "Project Manager" && <img src={projectM} class="card-img-top" alt={frontmatter.title} />}
                                        {frontmatter.title === "AgenDa" && <img src={AgenDa} class="card-img-top" alt={frontmatter.title} />}
                                        <div class="card-body text-center">
                                            <h5 class="card-title">{frontmatter.title}</h5>
                                            <p class="card-text">{frontmatter.description}</p>
                                            <a href={frontmatter.path} class="btn btn-primary">Voir le projet</a>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    <div className="row mt-4">
                        <div className="col-12">
                            <div class="card text-center border-0">
                                <div class="card-body custom-card">
                                    <h5 class="card-title">Affichage des projets par langage de programmation</h5>
                                    <p class="card-text">Si vous le souhaitez, vous pouvez afficher les projets par langages de programmation.</p>
                                    <a href="/tags" class="btn btn-primary">Voir la liste des langages</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Contact />
                <Footer />
            </div>
        </Layout >
    )
}

export const query = graphql`
    query HomepageQuery {
        allMarkdownRemark(
            sort: {order: DESC, fields: [frontmatter___date]}
            ) {
                edges {
                node {
                frontmatter {
                    title
                    path
                    image
                    date
                    description
                }
            }
        }
    }
}
 
`

export default page
