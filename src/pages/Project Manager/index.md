---
path: "/projectmanager"
date: "2020"
title: "Project Manager"
image: "projectM1"
tags: ["React", "GraphQL", "Markdown", "Javascript"]
description: "Projet realisé en stage de deuxième année pour l'entreprise Oktopod"
---

### Presentation du projet : 

La mission de ce projet consiste à développer une application web de gestion interne client / projet. Je suis chargé de la partie développement applicative ainsi que de la récupération des données depuis Apollo une API GraphQl.

#### Base de données :

La base de données est en PostgresSQL. Je me suis servi d'Apollo et de GraphQl pour aller chercher les informations nécessaires pour l'utilisateur connecté.
J'ai conçu un MCD permettant de répondre aux attentes demander.

![accueil](images/MCD.png)

#### Première partie :

Ma première mission était de mettre en place une page d'accueil recensant tous les projets auxquels la personne connecter à accès. Et que, lors du clique sur un projet, cela afficher toutes ses features.

![accueil](images/projectM1.png)

#### Seconde partie :

Ensuite, je devais faire une page Spécification, qui afficher les spécifications de chaque features (cette page était écrite en markdown)

![spe](images/projectM2.png)

#### Troisième partie :

J'ai mis en place une page qui afficher tous les bugs où toutes les question (même page, elle change en fonction si on clique sur bug ou question dans l'affichage des features)

![bugquestion](images/projectM3.png)

#### Quatrième partie :

Suite à la mise en place de la page de filtre bug ou questions (ci-dessus), il fallait une page permettant à l'utilisateur de chatter avec le développeur en charge du projet.
Ici le client pouvait poser sa question ou expliquer son bug. Le développeur peut ainsi aisément régler le problème.

![bugquestionchat](images/projectM4.png)
