import React from "react"
import Header from '../components/Header'

export default function Thanks() {
    return (
        <div>
            <Header />
            <div className="thanks clear-filter">
                <div className="thanks-image">
                    <div className="row h-100">
                        <div className="col-sm-12 my-auto">
                            <div className="text-center txt-accueilP">
                                Merci pour l'envoi du Formulaire !
                            </div>
                            <div className="text-center txt-accueil">
                                En espérant que le site vous a plu !
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}