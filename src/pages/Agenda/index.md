---
path: "/agenda"
date: "2020"
title: "AgenDa"
image: "agenda"
tags: ['PHP', 'HTML', 'CSS', 'SQL']
exerpt: "Un avant-goût de mon projet"
description: "Projet réalisé durant ma seconde année de BTS SIO"
---

### Debut du projet : 

Le principe de ce projet est de proposer un site PHP permettant à des entreprises ou des particuliers de faire un agenda directement depuis le site web. Nous sommes 2 à travailler sur les projets voici quelques photos :

![imgsite](images/agenda.png)

#### Base de données :

La base de données comporte 12 tables qui nous permettent de gérer les comptes et les dates dans l'agenda.
Voici une photo du mcd :

![mcd](images/MCD Agenda.png)

### Suite du projet : 

Dans la suite du projet, nous voulons ajouter une page ajout de compte et une page qui permet d'ajouter / modifier / supprimer une "ligne" de l'agenda. Je modifierai ce post le projet sera mieux avancé !

Vous pouvez aussi voir le code sur [github](https://github.com/Worgueno43/Agenda.git)

