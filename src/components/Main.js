import React from "react"
import file from "../img/file.png"
import folder from "../img/folder.png"


export default function Main() {
    const l = "{";
    return (
        <div>
            <div className="page-header clear-filter" id="accueil">
                <div className="page-header-image">
                    <div className="row h-50">
                        <div className="col-sm-12 my-auto">
                            <div className="text-center txt-accueil">
                                Bonjour, je suis
                            </div>
                            <div className="text-center txt-accueilP">
                                Yvan Thalamas
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container-fluid acontainer" id="apropos">
                <div className="row">
                    <div className="col-3 background-left">
                        <div>
                            <img src={folder} className="img-fluid icon ml-3 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">Langages maitrisées</p>
                        </div>
                        <div className="mt-4">
                            <img src={file} className="img-fluid icon ml-5 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">Javascript</p>
                        </div>
                        <div className="mt-4">
                            <img src={file} className="img-fluid icon ml-5 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">Html</p>
                        </div>
                        <div className="mt-4">
                            <img src={file} className="img-fluid icon ml-5 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">Css</p>
                        </div>
                        <div className="mt-4">
                            <img src={file} className="img-fluid icon ml-5 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">Sql</p>
                        </div>
                        <div className="mt-4">
                            <img src={file} className="img-fluid icon ml-5 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">Php</p>
                        </div>
                        <div className="mt-5">
                            <img src={folder} className="img-fluid icon ml-3 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">Framework maitrisées</p>
                        </div>
                        <div className="mt-4">
                            <img src={file} className="img-fluid icon ml-5 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">React.js</p>
                        </div>
                        <div className="mt-4">
                            <img src={file} className="img-fluid icon ml-5 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">Symfony</p>
                        </div>
                        <div className="mt-4">
                            <img src={file} className="img-fluid icon ml-5 d-inline" alt="Responsive image" />
                            <p className="white d-inline ml-3">Bootstrap</p>
                        </div>
                    </div>
                    <div className="col-9 background-right">
                        <div className="row">
                            <div className="col-1">
                                <div className="line-numbers pt-5">
                                    <p>&nbsp;1 &nbsp;2 &nbsp;3 &nbsp;4 &nbsp;5 &nbsp;6 &nbsp;7 &nbsp;8 &nbsp;9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44</p>
                                </div>
                                <div className="border-right ml-4"></div>
                            </div>
                            <div className="col-10 pt-4">
                            <div className="about-content-container pt-3">
                                <div className="comments">
                                    <p>/**</p>
                                    <p>*Apprendre à me connaître</p>
                                    <p>*Grâce à ce bout de code</p>
                                    <p>*/</p>
                                </div>
                                    <div className="about-main-section">
                                        <div className="about-list">
                                            <p><span className="couleur">const</span> A_Propos <span className="couleur"> = </span> [{l}</p>
                                            <div className="listinfo">
                                                <p className="tag-info">prenom<span className="couleur">:</span> <span className="couleurtxt">"Yvan"</span>,</p>
                                                <p className="tag-info">nom<span className="couleur">:</span> <span className="couleurtxt">"Thalamas"</span>,</p>
                                                <p className="tag-info">age<span className="couleur">:</span> <span className="couleurtxt">"20 ans"</span>,</p>
                                                <p className="tag-info">passions<span className="couleur">:</span> [</p>
                                                    <p className="passions">{l}</p>
                                                        <p className="insidepassions">name<span className="couleur">:</span> <span className="couleurtxt">"Handball"</span></p>
                                                    <p className="passions">},</p>
                                                    <p className="passions">{l}</p>
                                                        <p className="insidepassions">name<span className="couleur">:</span> <span className="couleurtxt">"Musiques"</span></p>
                                                    <p className="passions">},</p>
                                                    <p className="passions">{l}</p>
                                                        <p className="insidepassions">name<span className="couleur">:</span> <span className="couleurtxt">"Nouvelles technologies"</span></p>
                                                    <p className="passions">},</p>
                                                <p className="tag-info">],</p>
                                                <p className="tag-info">bio<span className="couleur">:</span> <span className="bio">"Actuellement en BTS SIO 2ème année. j'acquiers des compétences de developpement web depuis 2 ans maintenant. Je code principalement en HTML, CSS, PHP (Classique et Symfony) mais aussi depuis peu en Javascript. Grâce au stage proposé par ma formation, j'ai eu l'occasion de me pencher sur de nouveaux framework tel que React.js ou encore CodeIgniter"</span></p>
                                                <p>}]</p>
                                            </div>
                                        </div>
                                        <p><span className="couleur">export </span>{l} A_Propos };</p>   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}


    //     <div>
    //         <div id="apropos" className="test">
    //             <Box className="box--apropos">
    //                 <Box className="VSCODEM">
    //                     <img src={vscodelogo} className="logovscode" /> <p className="tools">File Edit Selection View Go Debug Terminal Help <span className="Titre">A_Propos.js - portfolio - Visual Studio Code</span></p>
    //                 </Box>
    //                 <Box className="toolsleft">
    //                     <img src={filel} className="logoleft" />
    //                     <img src={searchl} className="logoleft" />
    //                     <img src={sendl} className="logoleft" />
    //                     <img src={debugl} className="logoleft" />
    //                     <img src={appl} className="logoleft" />
    //                     <img src={vssharel} className="logoleft" />
    //                     <img src={paramsl} className="logoleftp" />
    //                 </Box>
    //                 <Box className="right rightbox">
    //                     <Box className="barreTitre">
    //                         <p className="titleAP"><img src={js} className="logo"></img>A_Propos.js</p>
    //                     </Box>
    //                     <Box className="soustitre">
    //                         <p className="subtitle">src > components > <img src={js2} className="logo2"></img>A_Propos.js</p>
    //                     </Box>
    //                     <Box className="contenu">
    //                         <div className="line-numbers">
    //                             <p>&nbsp;1 &nbsp;2 &nbsp;3 &nbsp;4 &nbsp;5 &nbsp;6 &nbsp;7 &nbsp;8 &nbsp;9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36</p>
    //                         </div>
    //                         <div className="border-right"></div>
    //                         <div className="about-content-container">
    //                             <div className="comments">
    //                                 <p>/**</p>
    //                             <p>*Apprendre à me connaître</p>
    //                             <p>*Grâce à ce bout de code</p>
    //                             <p>*//*</p>
    //                         </div>
    //                         <div className="about-main-section">
    //                             <ul className="about-list">
    //                                 <li>
    //                                     <p><span className="couleur">const</span> A_Propos <span className="couleur"> = </span> [{l}</p>
    //                                     <div className="listinfo">
    //                                         <p className="tag-info">prenom<span className="couleur">:</span> <span className="couleurtxt">"Yvan"</span>,</p>
    //                                         <p className="tag-info">nom<span className="couleur">:</span> <span className="couleurtxt">"Thalamas"</span>,</p>
    //                                         <p className="tag-info">age<span className="couleur">:</span> <span className="couleurtxt">"20 ans"</span>,</p>
    //                                         <p className="tag-info">passions<span className="couleur">:</span> [</p>
    //                                             <p className="passions">{l}</p>
    //                                                 <p className="insidepassions">name<span className="couleur">:</span> <span className="couleurtxt">"Handball"</span></p>
    //                                             <p className="passions">},</p>
    //                                             <p className="passions">{l}</p>
    //                                                 <p className="insidepassions">name<span className="couleur">:</span> <span className="couleurtxt">"Musiques"</span></p>
    //                                             <p className="passions">},</p>
    //                                             <p className="passions">{l}</p>
    //                                                 <p className="insidepassions">name<span className="couleur">:</span> <span className="couleurtxt">"Nouvelles technologies"</span></p>
    //                                             <p className="passions">},</p>
    //                                         <p className="tag-info">],</p>
    //                                         <p className="tag-info">bio<span className="couleur">:</span> <span className="bio">"Actuellement en BTS SIO 2ème année. j'acquiers des compétences de developpement web depuis 2 ans maintenant. Je code principalement en HTML, CSS, PHP (Classique et Symfony) mais aussi depuis peu en Javascript. Grâce au stage proposé par ma formation, j'ai eu l'occasion de me pencher sur de nouveaux framework tel que React.js ou encore CodeIgniter"</span></p>
    //                                         <p>}]</p>
    //                                     </div>
    //                                 </li>
    //                             </ul>
    //                             <p><span className="couleur">export </span>{l} A_Propos };</p>         
    //                         </div>
    //                     </div>
    //                 </Box>
    //             </Box>
    //         </Box>
    //     </div>
    // </div>