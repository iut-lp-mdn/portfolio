import React from "react"
import GitHub from "../img/github.png"
import downloadFile from '../download/Yvan Thalamas CV.pdf'

export default function Footer() {
    return (
        <div className="container-fluid footercontainer">
            <div className="row">
                <div className="col-4 mt-2">
                    <a className="white" href={downloadFile} download>Telecharger mon CV </a>{` `}
                </div>
                <div className="col-4 text-center pt-2">
                    <p className="white">© Yvan Thalamas</p>
                </div>
                <div className="col-4 text-right mt-2">
                    <p className="white d-inline mr-2">Mon GitHub </p>
                    <a href="https://github.com/Worgueno43"><img src={GitHub} className="img-fluid footericon d-inline" /></a>
                </div>
            </div>
        </div>
    )
}