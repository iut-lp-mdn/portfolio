import React from 'react';
function Box({ style, className = "", ...rest }) {
    return (
      <div
        className={`box ${className}`}
        style={{ paddingLeft: 20, ...style }}
        {...rest}
      />
    );
  }
  export { Box };
  