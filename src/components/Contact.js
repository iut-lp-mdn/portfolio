import React from "react"
import Thanks from '../pages/Thanks'

export default function Contact() {
    return (
        <div>
            <div className="container-fluid ccontainer" id="contact">
                <h3 className="text_b text-center white pt-5">Contactez-moi</h3>
                <div className="row mt-5">
                    <div className="col-1"></div>
                    <div className="col-10">
                        <div className="card formcard">
                            <div className="card-body text-center">
                                <form action="/Thanks" name="contact" method="POST" data-netlify-recaptcha="true" data-netlify="true">
                                    <input type="hidden" name="form-name" value="contact" />
                                    <div className="form-group">
                                        <label for="exampleInputName1">Nom</label>
                                        <input type="text" name="nom" className="form-control" id="exampleInputName1" aria-describedby="nameHelp" />
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputPrenom1">Prenom</label>
                                        <input type="text" name="prenom" className="form-control" id="exampleInputPrenom1" aria-describedby="prenomHelp" />
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputEmail1">Adresse E-Mail</label>
                                        <input type="email" name="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleFormControlTextarea1">Écrivez quelque chose</label>
                                        <textarea className="form-control" name="txtarea" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    </div>
                                    <div className="d-flex justify-content-center">
                                        <div className="g-recaptcha" data-sitekey="6LeBpd4UAAAAAKq_QXc7HwVqO50z6pwHGL7xXRXf" data-size="compact"></div>
                                    </div>
                                    <br />
                                    <div className="text-center">
                                        <input type="submit" className="btn btn-dark" value="Envoyer"></input>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-1"></div>
                </div>
            </div>
        </div>

    )
}